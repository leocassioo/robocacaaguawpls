#include <Servo.h> 

//Declarando os servos
Servo servoBase; 
Servo servoGarra; 
Servo servoDireita; 
Servo servoEsquerda;

//Pino analogigo do sensor de umidade
byte umidadePinAnalog = A4;
//Pino digital do sensor de umidade 
byte umidadePinVcc = 5;

void setup() 
{ 
servoBase.attach(9); 
servoGarra.attach(8); 
servoDireita.attach(7); 
servoEsquerda.attach(6);

// buzzer
pinMode(LED_BUILTIN, 13);
//sensor de umidade
pinMode(umidadePinVcc, OUTPUT);
digitalWrite(umidadePinAnalog, LOW);

  while (!Serial);
  delay(1000);
  Serial.begin(9600);

}

int ler_u_sensor() {
  digitalWrite(umidadePinVcc, HIGH);
  delay(500);
  int value = analogRead(umidadePinAnalog);
  digitalWrite(umidadePinVcc, LOW);
  return 1023 - value;
}

void loop() 
{ 
 int valorUmidade = ler_u_sensor();
  
  movBase(105); //calibrado MOVER BASE estado inicial
  movAltura(155); // calibrado 
  movDistancia(120);
  movAltura(130);
  movAltura(140);
  
  valorUmidade = ler_u_sensor();
  ReconhecerUmidade(valorUmidade);
  
  movDistancia(90);
  movAltura(180);
  movBase(140);
  movAltura(110);

  valorUmidade = ler_u_sensor();
  ReconhecerUmidade(valorUmidade);

  movAltura(160);
  movBase(45);
  movAltura(105);

  valorUmidade = ler_u_sensor();
  ReconhecerUmidade(valorUmidade);
  
  movAltura(180);
  movBase(180);
  
}

void ReconhecerUmidade(int valorUmidade)
{
  Serial.print("Umidade: ");
  Serial.println(valorUmidade);
  if (valorUmidade < 101 )
  {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);    
  }
  else
  {
    while (valorUmidade >= 101)
    {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(30);
      digitalWrite(LED_BUILTIN, LOW);
      delay(30);
      valorUmidade = ler_u_sensor();
    } 
  }
}


void movBase(int valor){
    delay(1000);
    servoBase.write(valor);
    delay(1000);
}
void movAltura(int valor){
    if(valor <= 180)
    {
      delay(2500);
      servoEsquerda.write(valor);
      delay(2500);
    }
}


void movDistancia(int valor){
    
    if(valor >= 0 && valor <= 180){
      delay(500);
      servoDireita.write(valor);
      delay(500);
    }
    
}
